/* Copyright (C) 2021. Logiciel �labor� par l��tat, via la Drees.

Auteurs : Eva Baradji, Drees.

Ce programme informatique a �t� d�velopp� par la Drees. Il permet de produire les illustrations sur la population g�n�rale de la publication n�1203
de la collection �tudes et r�sultats, ""Minima sociaux: des conditions de vie plus d�grad�es pour les b�n�ficiaires handicap�s".

Le texte et les tableaux de l�article peuvent �tre consult�s sur le site de la DREES : 
https://drees.solidarites-sante.gouv.fr/sites/default/files/2021-08/ER%201203.pdf

Ce programme utilise les donn�es de l'�ditions 2018 de l'enqu�te annuelle Statistiques sur les ressources et les conditions de vie (SRCV). 

Bien qu�il n�existe aucune obligation l�gale � ce sujet, les utilisateurs de ce programme sont invit�s � 
signaler � la DREES leurs travaux issus de la r�utilisation de ce code, ainsi que les �ventuels probl�mes 
ou anomalies qu�ils y rencontreraient, en �crivant � l�adresse �lectronique DREES-CODE@sante.gouv.fr

Ce programme a �t� ex�cut� pour la derni�re fois le 26/07/2021 avec la version 9.4 de SAS.


================
================================================================================

# LICENCE : Ce logiciel est r�gi par la licence �GNU General Public License� GPL-3.0. 
# https://spdx.org/licenses/GPL-3.0.html#licenseText

# � cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au chargement, � l'utilisation,
# � la modification et/ou au d�veloppement et � la reproduction du logiciel par l'utilisateur �tant donn� sa 
# sp�cificit� de logiciel libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
# d�veloppeurs et des professionnels avertis poss�dant des connaissances informatiques approfondies. 
# Les utilisateurs sont donc invit�s � charger et tester l'ad�quation du logiciel � leurs besoins dans des 
# conditions permettant d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
# � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

# Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris connaissance de la licence GPL-3.0, et que vous en avez accept� les termes.

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.

================================================================================================ */

libname srcv15 "T:\Ressources\INSEE\SRCV\SRCV 2004-2016";	
libname srcv18 "T:\Ressources\INSEE\SRCV\SRCV 2018";
/*============================================================================================================================== */	
/*CREATIONS DE VARIABLES SUR LA POPULATION GENERALE*/
/*============================================================================================================================== */

/*PAUVRETE EN CONDITONS DE VIE*/
	/*Table INDIVIDUS 2018*/
		data ind18;set srcv18.Individus18_diff;
		if AGE<16 then delete;
		eff=1;
		run;

	/*Table MENAGES 2018*/
		data men; set srcv18.Menages18_diff; run;
		proc sort data =men;by IDMENC_18;run;
		proc sort data=ind18;by IDMENC_18;run;
		data ind18_m;merge ind18 (in=a) men;if a;by IDMENC_18; run;

		/*ajout var densit� */
		data lgt2018
		(keep= idlogc /* identifiant du logement */ idmenc_18 /*identifiant m�nage */ 
		piecenb /* nombre de pieces d'habitation du logement*/ );
		set srcv18.menages18_diff;;
		run;*10 876;

		proc sort; by idmenc_18; run; 
		/*Recherche d'informations individuelles (type age des personnes du logement)*/
		data ind2018 (keep=/*idlogc  identifiant du logement */
		idmenc_18 /* identifiant du m�nage */ 
		sexe couple /* vie en couple 
		                  /* 1, oui avec une personne qui vit dans le logement
		                  /* 2, oui avec une personne qui ne vit pas dans le logement
		                  /* 3, non */
		age );
		set srcv18.individus18_diff; 
		run; *24698;

		proc sort; by idmenc_18; run;

		data lgtind2018; 
		merge lgt2018(in=z) ind2018; 
		by idmenc_18;
		if z;
		run;

		data t(keep=idmenc_18 mnoi cple celib18 g6 f6 g f );
		set lgtind2018; 
		by idmenc_18; 
		if first.idmenc_18 then do; 
		cple=0; 
		celib18=0; 
		g6=0; 
		f6=0; 
		g=0; 
		f=0 ; 
		end; 
		if couple in ('1') then cple+1;
		else if couple not in ('1') and age>18 then celib18+1;
		else do; 
		if sexe='1' then do; 
		g+1; 
		if age<7 then g6+1; 
		end;
		else do; 
		f+1; 
		if age<7 then f6+1; 
		end; 
		end; 
		if last.idmenc_18 then do;
		mnoi=1 +ceil(cple/2) + celib18 + ceil(g/2) + ceil(f/2);
		if (g6>0)*(f6>0)*(mod(g,2)=1)*(mod(f,2)=1) then mnoi=mnoi-1;
		output t; 
		end; 
		run;

		data densite2018;
		merge lgt2018 t;
		by idmenc_18;
		* variables de peuplement;
		if piecenb > mnoi+2 then kip='0';
		else if piecenb = mnoi+2 then kip='1';
		else if piecenb = mnoi+1 then kip='2'; 
		else if piecenb = mnoi then kip='3';
		else if piecenb = mnoi-1 then kip='4';
		else if piecenb < mnoi-1 then kip='5';
		* indicateur de surpeuplement;
		dens=(kip in ('4' '5'));
		run;

		proc sort data=densite2018; by idmenc_18; run;
		proc sort data=ind18_m; by idmenc_18; run;
		data ind18_m; merge ind18_m(in=a) densite2018; by idmenc_18;if a ; run;



		data ind18_m;set ind18_m ;
		
			if PCDVFR="1" then p_cond=100; else p_cond=0; 

		/*CREATION DES VARIABLES SUR LES 4 DIMENSIONS DE LA PAUVRETE EN CONDITONS DE VIE*

			/*Restriction de consommation*/
			if TEMP ="2"  then do; nb_temp =1; p_temp=100;  end;  else do; nb_temp =0; p_temp=0; end;
			if REPAS="1" then do; nb_repas =1;  p_repas=100;  end; else do; nb_repas =0; p_repas=0; end;
			if MEUB ="2" then do; nb_meub =1; p_meub=100;  end; else do; nb_meub =0; p_meub=0; end;
			if VET  ="2" then do; nb_vet =1; p_vet=100;end; else do; nb_vet =0; p_vet=0; end;
			if VIAND="2" then do; nb_viand =1; p_viand=100;  end; else do; nb_viand  =0; p_viand =0; end;
			if RECEP="2" then do; nb_recep =1; p_recep=100;  end; else do; nb_recep =0; p_recep=0; end;
			if CAD  ="2" then do; nb_cad =1;  p_cad=100;  end; else do; nb_cad =0; p_cad=0; end;
			if CHAUS="2" then do;nb_chaus =1;  p_chaus=100;  end; else do; nb_chaus =0; p_chaus=0; end;
			if vac="2" then do; nb_vac=1; p_vac=100;  end; else do; nb_vac =0; p_vac=0; end;
			restri=sum (of nb_temp nb_repas nb_meub nb_vet nb_viand nb_recep nb_cad nb_chaus nb_vac); 
			if restri>=4 then restri_conso="1"; else restri_conso="0";
			if restri_conso="1" then p_restri_conso=100; else p_restri_conso=0;

			/*Insuffisance de ressources */
			if REMBPA  ="3" then do ; nb_rembpa=1; p_rembpa=100; end; else do p_rembpa=0;end;
			if REMVAR  ="1" then do ; nb_remvar=1; p_remvar=100; end; else do p_remvar=0;end;
			if EQUIL   ="2" then do ; nb_EQUIL=1; p_EQUIL=100; end; else do p_EQUIL=0;end;
			if PUISEC  ="1" then do ; nb_PUISEC=1; p_PUISEC=100; end; else do p_PUISEC=0;end;
			if TYPRF_G ="1" then do ; nb_TYPRF_G=1; p_TYPRF_G=100; end; else do p_TYPRF_G=0;end;
			if NIVACTB in ("1" ) then do ; nb_NIVACTB=1; p_NIVACTB=100; end; else do p_NIVACTB=0;end;

			if NIVACTB in ("1" "2" ) then do ; nb_diff=1; p_diff=100; end; else do p_diff=0;end;
			if NIVACTB in ( "2" ) then do ; nb_diff2=1; p_diff2=100; end; else do p_diff2=0;end;

			insuff=sum (of nb_rembpa nb_remvar nb_EQUIL nb_PUISEC nb_TYPRF_G /*nb_NIVACTB*/ nb_diff); 
			if insuff>=3 then insuff_res="1"; else insuff_res="0";
			if insuff_res="1" then p_insuff_res=100; else p_insuff_res=0;

			/*Difficult�s de paiement*/
			if IPELEC in ("1" "2")  or IPIMP in ("1" "2")  or IPLOY in ("1" "2") then p_ip=100; else p_ip=0; 

			/*Difficult�s de logement*/
			if BAIN ="2"   then do; nb_bain=1;  P_BAIN=100; end ; else do;  P_BAIN =0;end;
			if WC ="2"   then  do;  nb_wc=1; P_WC=100;  end ; else do;  P_WC =0;end;
			if EAUCHAUD ="2"   then  do;  nb_EAUCHAUD=1; P_EAUCHAUD=100;  end ; else do;  P_EAUCHAUD =0;end;
			if CHAUF ="2"   then  do;  nb_chauf=1; P_CHAUF=100;  end ; else do;  P_CHAUF =0;end;
			if PETIT ="1"   then  do;  nb_petit=1; P_PETIT=100;  end ; else do;  P_PETIT =0;end;
			if DIFCHAUF ="1"   then  do;  nb_difchauf=1; P_DIFCHAUF=100;  end ; else do;  P_DIFCHAUF =0;end;
			if TOIT ="1"   then  do;  nb_toit=1; P_TOIT=100;  end ; else do;  P_TOIT =0;end;
			if BRUIT ="1"   then  do;  nb_bruit=1; P_BRUIT=100;  end ; else do;  P_BRUIT =0;end;
			if dens ="1"   then  do;  nb_dens=1; P_dens=100;  end ; else do;  P_dens =0;end;
			loge=sum (of nb_bain nb_wc nb_EAUCHAUD nb_chauf nb_petit nb_difchauf nb_toit nb_bruit nb_dens); 
			if loge>=3 then diff_logt="1"; else diff_logt="0";
			if diff_logt="1" then p_diff_logt=100; else p_diff_logt=0;


		run;

/*RELATIONS SOCIALES _ SRCV 2015*/
		data ind15;set srcv15.Individus15_diff;
		if AGE<16 then delete;
		eff=1;

		if contfam in ("1" "2"  "3" "4") or retfam  in ("1" "2" "3"  "4") then p_contact_fam=100; else p_contact_fam=0;

		if contami in ("1" "2" "3" "4") or retami  in ("1" "2" "3" "4" ) then p_contact_ami=100; else p_contact_ami=0;

		if contami not in ("1" "2"  "3" "4") and retami  not in ("1" "2" "3" "4" ) and contfam not in ("1" "2" "3" "4" ) and  retfam  not in ("1" "2" "3" "4" )  then isole ="1"; else isole="0";
		if isole="1" then p_isole=100; else p_isole=0;

		if  retfam  in ("7") then p_no_fam=100; else p_no_fam=0;
		if retami  in ("7" ) then p_no_ami=100; else p_no_ami=0;

		run;

/*============================================================================================================================== */
/*FORMATS*/
/*============================================================================================================================== */
proc format;
value $dim (multilabel notsorted)
	"1" ="Oui, fortement limit�(e)"
	 "2", "3"," " ="non";

value $sexe
	"2" = "Femmes"
	"1"= "Hommes";

value age
	16-64 ="Moins de 65 ans"
	65-high= "65 ans et plus";

value tr_age2_
	low-24 ="Moins de 25 ans"
	25-34 ="25 � 34 ans"
	35-44 ="35 � 44 ans"
	45-54 ="45 � 54 ans"
	55-64 ="55 � 64 ans"
	65-high="65 ans et plus";


value $typmen
	"1" = "Personne seule"
	"2" = "Famille monoparentale"
	"3" = "Couple sans enfant"
	"4" = "Couple avec au moins un enfant"
	"5" = "Autre type de m�nage (m�nage complexe)";

value $situa(multilabel notsorted)
	"1"=" Occupe un emploi"
	"4"=" Au ch�mage (inscrit(e) ou non au P�le Emploi (ex ANPE))"
	"6", "7", "8"= "Inactifs"
	"7"=" dont inactif(ve) pour cause d�invalidit�"
	"5"=" Retrait�(e) ou retir�(e) des affaires ou en pr�retraite"
	"2"=" Apprenti(e) sous contrat ou stagiaire r�mun�r�(e)"
	"3"=" �tudiant(e), �l�ve, en formation ou stagiaire non r�mun�r�(e)";

value $situa_ag
	"1"=" Occupe un emploi"
	"2"=" Apprenti(e) sous contrat ou stagiaire r�mun�r�(e)"
	"4"=" Au ch�mage (inscrit(e) ou non au P�le Emploi (ex ANPE))"
	"5"=" Retrait�(e) ou retir�(e) des affaires ou en pr�retraite"
	"6", "7", "8" =" Inactifs"
	"3"=" �tudiant(e), �l�ve, en formation ou stagiaire non r�mun�r�(e)";

value $saneta (multilabel notsorted)
	"1","2"="Tr�s bon � bon"
	"3 "=" Assez bon"
	"4", "5" ="Mauvais � tr�s mauvais"
	"4"="  Mauvais"
	"5"="  Tr�s mauvais";

value $malgrav (multilabel notsorted)
	"1"="Maladie chronique"
	other=  "non";

quit;

/*============================================================================================================================== */
/*DONNNEES SUR LA POPULATION GENERALE POUR TABLEAU DE L'ER*/
/*============================================================================================================================== */

/*Tableau 1 - Proportions des personnes handicap�es au sens du GALI parmi les b�n�ficiaires de minima sociaux et dans la population g�n�rale*/
	proc tabulate data=ind18;
	class  dim age / preloadfmt missing  order=data mlf  ;
	var  eff /weight = pb040;
	table  (dim="" all="Ensemble"),(age="" all="Ensemble")*(eff=""*f=8.1)  /printmiss;
	format  age age.  dim $dim.; run;
	run;

/*Graphique 1  - Part des b�n�ficiaires expos�s � la pauvret� en conditions de vie et ses quatre dimensions*/
	proc tabulate data=ind18_m;
	class  age   dim  / preloadfmt missing  order=data mlf  ;
	var   p_cond p_restri_conso p_insuff_res  p_diff_logt p_ip/weight = PB040;
	table   (p_cond ="Pauvret� en conditions de vie"
	p_restri_conso ="Restriction de consommation" 
	p_insuff_res  ="Insuffisance de ressources"
	p_ip="Retard de paiement"
	p_diff_logt ="Difficult�s de logement")*(mean=' '*f=8.1 ) , 
	(dim="" all ="Ensemble" )/printmiss;
	format  age age.  dim $dim.; run;
	run; 

/*Graphique 2 - �tat de sant� d�clar� par les b�n�ficiaires de minimas sociaux et dans la population g�n�rale*/
	proc tabulate data=e.ind;
	class saneta malgrav dim / preloadfmt /*missing*/  order=data mlf  ;
	var  eff/weight = pb040;
	table   (saneta="" malgrav="" all="Total"), 
	(dim="" all="Ensemble")*(eff=""*sum=' '*f=8.0 )  /printmiss;
	format saneta $saneta.  malgrav $malgrav.    dim $dim.; run;
	run; 

/*Graphique 3 - Relations familiales et amicales au cours des douze derniers mois des b�n�ficiaires de minima sociaux et dans la population g�n�rale */
	proc tabulate data=ind15;
	class  dim age/ preloadfmt missing  order=data mlf  ;
	var  p_no_fam p_no_ami p_contact_fam p_contact_ami p_isole/weight = Pb040;
	table(
		p_no_fam="Pas de famille"
		p_no_ami="Pas d'ami"
		p_contact_fam="Avoir vu ou communiqu� avec sa famille au moins une fois par mois"
		p_contact_ami="Avoir vu ou communiqu� avec ses amis au moins une fois par mois"
		p_isole ="Personnes isol�es de leur entourage et de leur famille")*(mean=' '*f=8.1 )  , 
		(dim="" all="Ensemble" )  /printmiss;
	format dim $dim.; run;
	run; 


/*Tableau compl�mentaire A - Caract�ristiques des personnes handicap�es parmi les b�n�ficiaires de minima sociaux et dans l�ensemble de la population*/
	proc tabulate data=ind18_m;
	class sexe age situa  dim typmen5 / preloadfmt missing  order=data mlf  ;
	var  eff/weight = pb040;
	table   ( sexe ="" age="" typmen5 ="" situa="" all="Ensemble"), 
	(dim="" all="Ensemble")*(eff=""*sum=' '*f=8.0 )  /printmiss;
	format sexe $sexe. csp $csp.  age tr_age2_. typmen5 $typmen. situa $situa.  dim $dim.; run;
	run;
	/*Age moyens et m�dians*/
	proc sort data=ind18_m; by dim; run;
	proc univariate data=ind18_m; var age;by dim; weight pb040 ; format  dim $dim.;run;


/*Tableau compl�mentaire C�- Part des b�n�ficiaires pauvres en conditions de vie parmi les b�n�ficiaires de minima sociaux et dans la population g�n�rale*/
	proc tabulate data=ind18_m;
	class  age   dim  / preloadfmt missing  order=data mlf  ;
	var   p_cond /weight = PB040;
	table   (p_cond ="Pauvret� en conditions de vie"
	)*(mean=' '*f=8.1 ) , 
	(dim="" all ="Ensemble" )*(age="" all="Ensemble")/printmiss;
	format  age age.  dim $dim. age age.; run;
	run; 
