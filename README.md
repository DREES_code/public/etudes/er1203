Ce dossier fournit les programmes SAS permettant de produire les illustrations de l'Études et résultats n° 1203de la DREES : "Bénéficiaires de minima sociaux : des conditions de vie plus dégradées pour les personnes handicapées".

Lien vers l'étude: https://drees.solidarites-sante.gouv.fr/sites/default/files/2021-08/ER%201203.pdf

Présentation de la DREES : La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères.
https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees

Source de données : enquête auprès des bénéficiaires de minima sociaux (BMS) de 2018 (Drees) (https://drees.solidarites-sante.gouv.fr/sources-outils-et-enquetes/01-enquete-sur-les-beneficiaires-de-minima-sociaux-bms) et enquête annuelle Statistiques sur les ressources et les conditions de vie (SRCV) de 2018 (Insee). Traitements : Drees. 

Le code de cet "Études et résultats" est constitué de 2 programmes :
•	le programme « ER1203_données sur les BMS » produit les données sur les bénéficiaires de minima sociaux à partir de l’enquête BMS dans l’ensemble des tableaux et graphiques de l’étude
•	le programme « ER1203_données sur la population » produit les données sur l’ensemble de la population dans les tableaux1, complémentaire A et complémentaire C et les graphiques 1, 2 et 3 de l’étude

Les programmes ont été exécutés pour la dernière fois avec le logiciel SAS version 9.4 , le 26/07/2021.

