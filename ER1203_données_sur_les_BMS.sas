/* Copyright (C) 2021. Logiciel �labor� par l��tat, via la Drees.

Auteurs : Eva Baradji, Drees.

Ce programme informatique a �t� d�velopp� par la Drees. Il permet de produire les illustrations sur les b�n�ficiaires de minima sociaux de la publication n�1203
de la collection �tudes et r�sultats, "Minima sociaux: des conditions de vie plus d�grad�es pour les b�n�ficiaires handicap�s".

Le texte et les tableaux de l�article peuvent �tre consult�s sur le site de la DREES : 
https://drees.solidarites-sante.gouv.fr/sites/default/files/2021-08/ER%201203.pdf

Ce programme utilise les donn�es des �ditions 2018 de l'enqu�te aupr�s des b�n�ficiaires de minima sociaux (BMS) de la DREES.

Bien qu�il n�existe aucune obligation l�gale � ce sujet, les utilisateurs de ce programme sont invit�s � 
signaler � la DREES leurs travaux issus de la r�utilisation de ce code, ainsi que les �ventuels probl�mes 
ou anomalies qu�ils y rencontreraient, en �crivant � l�adresse �lectronique DREES-CODE@sante.gouv.fr

Ce programme a �t� ex�cut� pour la derni�re fois le 26/07/2021 avec la version 9.4 de SAS.


================
================================================================================

# LICENCE : Ce logiciel est r�gi par la licence �GNU General Public License� GPL-3.0. 
# https://spdx.org/licenses/GPL-3.0.html#licenseText

# � cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s au chargement, � l'utilisation,
# � la modification et/ou au d�veloppement et � la reproduction du logiciel par l'utilisateur �tant donn� sa 
# sp�cificit� de logiciel libre, qui peut le rendre complexe � manipuler et qui le r�serve donc � des 
# d�veloppeurs et des professionnels avertis poss�dant des connaissances informatiques approfondies. 
# Les utilisateurs sont donc invit�s � charger et tester l'ad�quation du logiciel � leurs besoins dans des 
# conditions permettant d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
# � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.

# Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris connaissance de la licence GPL-3.0, et que vous en avez accept� les termes.

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.

================================================================================================ */

libname bms "";/*Ajouter chemin pour atteindre la base bms.bms2018_partenaires*/
OPTION FMTSEARCH=(work.formats library) NOFMTERR ;
/*============================================================================================================================== */
/* Cr�ation de variables pour l'�tude*/
/*============================================================================================================================== */
data base_etude_bms; set bms.bms2018_partenaires; 
eff=1;
/*IDENTIFICATION DES BMS*/
	/*Cr�ation de la variable "ms" qui identie les b�n�ficiaires de minima sociaux (hors personnes qui b�n�ficient uniquement de la prime d'activit�)*/
	      ms=1;
	      if  typdet='PA' then ms=2;  


/* LIMITATIONS FONCTIONNELLES*/
	/*Indicatrices : avoir au moins une difficult� s�v�re ou mod�r�e / avoir au moins une difficult� s�v�re*/
		/* Toutes difficult�es s�v�res ou mod�r�es */
		if hc6 in (2 3 4) or hc7 in (2 3 4) or hc8 in (2 3 4) or hc9 in (2 3 4) or hc10 in (2 3 4) or hc11  in (2 3 4) or hc14  
		in (2 3 4) or hc13 in (2 3 4) then diff_tot=1;else diff_tot=0;
		/*s�v�re**/
		 if hc6 in (3 4) or hc7 in (3 4) or hc8 in (3 4) or hc9 in (3 4) or hc10 in (3 4) or hc11  in (3 4) or hc14  in (3 4) 
		or hc13 in (3 4) then diff_sev=1; else diff_sev=0;

		 /*Difficult�s sensoriels
		hc6 ="Difficult�s pour voir" 
		hc7 ="Difficult�s � entendre dans une pi�ce silencieuse" 
		hc8 ="Difficult�s � entendre dans une pi�ce bruyante" */
		 if hc6 in (3 4) or hc7 in (3 4) or hc8 in (3 4) then diff_senso_sev=1;else diff_senso_sev=0;/*s�v�re*/
		 if hc6 in (2 3 4) or hc7 in (2 3 4) or hc8 in (2 3 4) then diff_senso=1;else diff_senso=0;/*s�v�re et mod�r�e*/

		/*Difficult�s physiques
		hc9="Difficult�s � marcher 500m" 
		hc10="Difficult�s � monter ou descendre des marches" 
		hc14 ="Difficult�s � macher"  
		hc13 ="Difficult�s avec dentier" */
		 if hc9 in (3 4) or hc10 in (3 4) or hc14 in (3 4) or hc13 in (3 4) then diff_phy_sev=1;else diff_phy_sev=0;
		 if hc9 in (2 3 4) or hc10 in (2 3 4) or hc14 in (2 3 4) or hc13 in (2 3 4) then diff_phy=1;else diff_phy=0;

		/*Difficult�s cognitives
		hc11 ="Difficult�s � se souvenir"*/
		if hc11 in (3 4) then diff_cog_sev=1;else diff_cog_sev=0;
		if hc11 in (2 3 4) then diff_cog=1;else diff_cog=0;

	/*Variables sur les limitations (globales et d�taill�es) � utiliser dans une proc tabulate*/
		if diff_tot=1 then p_diff_tot=100; else p_diff_tot=0;
		if diff_sev=1 then p_diff_sev=100; else p_diff_sev=0;

		if diff_senso=1 then p_diff_senso_tot=100; else p_diff_senso_tot=0;
		if diff_senso_sev=1 then p_diff_senso_sev=100; else p_diff_senso_sev=0;

		if diff_phy=1 then p_diff_phy_tot=100; else p_diff_phy_tot=0;
		if diff_phy_sev=1 then p_diff_phy_sev=100; else p_diff_phy_sev=0;

		if hc6 in (2 3 4) then p_voir=100; else p_voir=0;
		if hc6 in (3 4) then pbcp_voir=100; else pbcp_voir=0;

		if hc7 in (2 3 4) then p_ents=100; else p_ents=0;
		if hc7 in (3 4) then pbcp_ents=100; else pbcp_ents=0;

		if hc8 in (2 3 4) then p_entb=100; else p_entb=0;
		if hc8 in (3 4) then pbcp_entb=100; else pbcp_entb=0;

		if hc9 in (2 3 4) then p_march=100; else p_march=0;
		if hc9 in (3 4) then pbcp_march=100; else pbcp_march=0;

		if hc10 in (2 3 4) then p_esca=100; else p_esca=0;
		if hc10 in (3 4) then pbcp_esca=100; else pbcp_esca=0;

		if hc11 in (2 3 4) then p_souv=110; else p_souv=0;
		if hc11 in (3 4) then pbcp_souv=100; else pbcp_souv=0;

		if hc14 in (2 3 4) or hc13 in (2 3 4) then p_mach=100; else p_mach=0;
		if hc14 in (3 4) or hc13 in (3 4) then pbcp_mach=100; else pbcp_mach=0;



	/*Cumul des difficult�s*/
		/*S�v�res*/
		if diff_senso_sev=1 and diff_phy_sev=0 and diff_cog_sev=0 then var_diff_sev="Difficult�s sensorielles uniquement     ";
		if diff_senso_sev=1 and diff_phy_sev=1 and diff_cog_sev=0 then var_diff_sev="Difficult�s sensorielles et physiques";
		if diff_senso_sev=1 and diff_phy_sev=0 and diff_cog_sev=1 then var_diff_sev="Difficult�s sensorielles et cognitives";

		if diff_senso_sev=0 and diff_phy_sev=1 and diff_cog_sev=0 then var_diff_sev="Difficult�s physiques uniquement";
		if diff_senso_sev=0 and diff_phy_sev=1 and diff_cog_sev=1 then var_diff_sev="Difficult�s physiques et cognitives";

		if diff_senso_sev=0 and diff_phy_sev=0 and diff_cog_sev=1 then var_diff_sev="Difficult�s cognitives uniquement";

		if diff_senso_sev=1 and diff_phy_sev=1 and diff_cog_sev=1 then var_diff_sev="3 difficult�s";

		if diff_senso_sev=0 and diff_phy_sev=0 and diff_cog_sev=0 then var_diff_sev="Aucune difficult�";

		/*S�v�res et mod�r�es*/
		if diff_senso=1 and diff_phy=0 and diff_cog=0 then var_diff="Difficult�s sensorielles uniquement     ";
		if diff_senso=1 and diff_phy=1 and diff_cog=0 then var_diff="Difficult�s sensorielles et physiques";
		if diff_senso=1 and diff_phy=0 and diff_cog=1 then var_diff="Difficult�s sensorielles et cognitives";

		if diff_senso=0 and diff_phy=1 and diff_cog=0 then var_diff="Difficult�s physiques uniquement";
		if diff_senso=0 and diff_phy=1 and diff_cog=1 then var_diff="Difficult�s physiques et cognitives";

		if diff_senso=0 and diff_phy=0 and diff_cog=1 then var_diff="Difficult�s cognitives uniquement";

		if diff_senso=1 and diff_phy=1 and diff_cog=1 then var_diff="3 difficult�s";

		if diff_senso=0 and diff_phy=0 and diff_cog=0 then var_diff="Aucune difficult�";

/*PAUVRETE EN CONDITIONS DE VIE ET SES DIMENSIONS DETAILLEES*/
	/*Variables pour proc tabulate*/
		/*Pauvret� en conditions de vie + 4 dimensions*/
		if PCDV=1 then p_pcdv=100; else p_pcdv=0;
		if difflogt=1 then p_difflogt=100; else p_difflogt=0;
		if diffbudg=1 then p_diffbudg=100; else p_diffbudg=0;
		if retpai=1 then p_retpai=100; else p_retpai=0;
		if restrconso=1 then p_restrconso=100; else p_restrconso=0;

		/* D�tail de chacune des dimensions*/
		/*Restrictions de consommation*/
		if conso1=1 then p_temp=100; else p_temp=0;
		if conso2=1 then p_vac=100; else p_vac=0;
		if conso3=1 then p_meub=100; else p_meub=0;
		if conso4=1 then p_vet=100; else p_vet=0;
		if conso5=1 then p_viand=100; else p_viand=0;
		if conso6=1 then p_rec=100; else p_rec=0;
		if conso7=1 then p_kdo=100; else p_kdo=0;
		if conso8=1 then p_chauss=100; else p_chauss=0;
		if conso9=1 then p_repas=100; else p_repas=0;
		/*Insuffisance de ressources*/
		if budg1=1 then p_remb=100; else p_remb=0;
		if budg2=1 then p_dec =100; else p_dec=0;
		if budg3=1 then p_couv=100; else p_couv=0;
		if budg4=1 then p_fin =100; else p_fin=0;
		if budg5=1 then p_eco =100; else p_eco=0;
		if budg6=1 then p_dif =100; else p_dif=0;
		/*Difficult�s de logement*/
		if logt1=1 then p_surpeup =100; else p_surpeup=0;
		if logt2=1 then p_sdb     =100; else p_sdb=0; 
		if logt3=1 then p_wc      =100; else p_wc=0;
		if logt4=1 then p_hot     =100; else p_hot=0;
		if logt5=1 then p_chauf   =100; else p_chauf=0;
		if logt6=1 then p_petit    =100; else p_petit=0;
		if logt7=1 then p_difchauf=100; else p_difchauf=0;
		if logt8=1 then p_hum     =100; else p_hum=0;
		if logt9=1 then p_bruit   =100; else p_bruit=0;
		/*retard de paiement*/
		if retard1=1 then p_factures=100; else p_factures=0;
		if retard2=1 then p_loyers  =100; else p_loyers=0;
		if retard3=1 then p_impot      =100; else p_impot=0;


/*RELATIONS SOCIALES*/
		/*Variables pour tabulate*/
		if hb2  in ( 5)  then p_nofami=100; else p_nofami=0;/*Pas de famille*/
		if hb5 in (5)  then p_noami=100; else p_noami=0;/*Pas d'ami*/
		if hb2  in (1 2) or hb3 in (1 2) then p_fami=100; else p_fami=0;/*Avoir vu ou communiqu� avec sa famille au moins une fois par mois*/
		if hb5 in (1 2) or hb6 in (1 2) then p_ami=100; else p_ami=0;/*Avoir vu ou communiqu� avec ses amis au moins une fois par mois*/
		if hb2  not in (1 2  -1 -2) and hb3 not in (1 2 -1 -2) and hb5 not in (1 2  -1 -2) and hb6 not in (1 2  -1 -2)  then p_isol=100; else p_isol=0; /*moins d�une relation (rencontre ou contact)avec famille et amis par mois sur l�ann�e �coul�e*/

/*RENONCEMENT AUX SOINS*/
		/*Variables pour tabulate*/
		if f6=1 then p_renoncmed=100; else p_renoncmed=0; 
		if f7=1 then p_renoncdent=100; else p_renoncdent=0;
run;
/*============================================================================================================================== */
/*FORMATS*/
/*============================================================================================================================== */

proc format ;

value handi
	1="Personnes handicap�es"
	other ="Personnes non handicap�es";


value tr_age1_
	low-64 ="Moins de 65 ans"
	65-high="65 ans et plus";

/*Etat de sant�*/
value hc1_
	1,2 = "Bon � tr�s bon"
	3="Assez bon"
	4,5="Mauvais � tr�s mauvais"
	other="NR";

value ouinon
	1="Oui"
	2="Non"
	3="Sans objet"
	-1="NSP"
	-2="Refus";

value sexe
	1="Hommes"
	2="Femmes";

value tr_age2_
	low-24 ="Moins de 25 ans"
	25-34 ="25 � 34 ans"
	35-44 ="35 � 44 ans"
	45-54 ="45 � 54 ans"
	55-64 ="55 � 64 ans"
	65-high="65 ans et plus";

value typmen
	1 = "Personne seule"
	2 = "Famille monoparentale"
	3 = "Couple sans enfant"
	4 = "Couple avec enfant(s)"
	5 = "M�nage Complexe";

value situemp 
	   -3 = '(Non concern�)'
	   -2 = '(Refus)'
	   -1 = '(NSP)'
	   1 = 'Vous exercez une profession vous aidez un membre de votre famille dans son travail m�me sans �tre r�mun�r�(e) vous �tes apprenti(e), stagiaire r�mun�r�(e)'
	   2 = 'Vous �tes ch�meur(euse) (INSCRIT OU NON A POLE EMPLOI)'
	   3 = 'Vous �tes �tudiant(e), �l�ve, en formation, en stage non r�mun�r�'
	   4 = 'Vous �tes femme ou homme au foyer'
	   5 = 'Vous �tes retrait�(e) ou pr�retrait�(e)'
	   6 = ' Autre inactif (Y COMPRIS LES PERSONNES NE TOUCHANT QU�UNE PENSION DE REVERSION ET LES PERSONNES INVALIDES)';

value freq_
	1,2 ="Tout le temps, la plupart du temps"
	3="Plus de la moiti� du temps"
	4,5="Moins de la moiti� du temps, de temps en temps"
	other="NR";

value hb7_ /*solitude*/
	1 = "Oui souvent"
	2 = "Oui parfois"
	3 ="Non"
	-1 ="NSP"
	-2 ="Refus";
quit;


/*============================================================================================================================== */
/*DONNEES POUR TABLEAUX ET GRAPHIQUE DE L'ER*/
/*============================================================================================================================== */

/*Tableau 1 - Proportions des personnes handicap�es au sens du GALI parmi les b�n�ficiaires de minima sociaux (BMS) et dans la population g�n�rale*/
	/* Ensemble des BMS (dans le tableau : ligne de donn�es 1, colonnes 1, 2 et 3)*/
		proc freq data=base_etude_bms ;
		table hc3*age /missing;weight poids_fin_ind_ms_fe;
		where ms=1;
		format hc3 handi. age tr_age1_.;run;

	/*BMS par prestation (dans le tableau : lignes de donn�es 2, 3, 4 et 5, colonnes 1, 2 et 3)*/
		proc tabulate data=base_etude_bms;
		class hc3 age aah17 minv17 rsa17 ass17/ preloadfmt missing order=data mlf ;
		var  eff/weight=poids_fin_ind_prest_fe ;
		table (aah17="Allocation aux adultes handicap�s (AAH)" minv17="Minimum vieillesse" rsa17="Revenu de solidarit� active (RSA)" ass17="Allocation de solidarit� sp�cifique"  ), 
		(hc3="" all="Total")*  (age="" all="ensemble")*( eff=' '*f=8.1 )/printmiss; 
		where ms=1;
		format hc3 handi. age tr_age1_.;
		run; 

	/*Limitations des BAAH et BMINV (dans le tableau : lignes 2 et 3, colonnes 4 et 5) */
		proc tabulate data=base_etude_bms;
		class diff_tot diff_sev diff_mod/ preloadfmt missing order=data mlf ;
		var eff  /weight= poids_fin_ind_prest_FE;
		table ( diff_tot="Au moins une limitation s�v�re ou mod�r�e" diff_sev="Au moins une limitation s�v�re" )  , 
		( eff=""*f=8.0 ) /printmiss; 
		 where aah17=1 ;
		run; 
		proc tabulate data=base_etude_bms;
		class diff_tot diff_sev diff_mod/ preloadfmt missing order=data mlf ;
		var eff  /weight= poids_fin_ind_prest_FE;
		table ( diff_tot="Au moins une limitation s�v�re ou mod�r�e" diff_sev="Au moins une limitation s�v�re" )  , 
		( eff=""*f=8.0 ) /printmiss; 
		 where minv17=1;
		run; 

	/* Les chiffres de la colonne "B�n�ficiaires hors champ en raison de leur lieu de r�sidence ou de leur �tat de sant�" ont �t� r�cup�r�s aupr�s du BLEX*/



/*Graphique 1 : Part des b�n�ficiaires expos�s � la pauvret� en conditions de vie et ses quatre dimensions*/

	/*Part des b�n�ficiaires expos�s � la pauvret� en conditions de vie*/
	proc tabulate data=base_etude_bms;
	class hc3 / preloadfmt missing order=data mlf ;
	var p_pcdv /weight= poids_fin_ind_ms_fe /* poids_fin_ind_ms_fe*/;
	table ( p_pcdv  ="Pauvret� en conditions de vie" *mean=' '*f=8.1 ) ,
	(hc3="" all="Total")/printmiss; 
	where pcdv in (0 1) and ms=1;
	format hc3 handi.;
	run; 
	/*Part des b�n�ficiaires expos�s � des restrictions de consommation*/
	proc tabulate data=base_etude_bms;
	class hc3 / preloadfmt missing order=data mlf ;
	var   p_restrconso/weight= poids_fin_ind_ms_fe  ;
	table (p_restrconso="Restriction de consommation"  *mean=' '*f=8.1 ) , (hc3="" all="Total")/printmiss; 
	where restrconso in (0 1) and ms=1;
	format  hc3 handi.;
	run; 
	/*Insuffisances de ressources*/
	proc tabulate data=base_etude_bms;
	class hc3 / preloadfmt missing order=data mlf ;
	var  p_diffbudg /weight= poids_fin_ind_ms_fe  ;
	table (p_diffbudg="Insuffisance de ressources"  *mean=' '*f=8.1 ) , (hc3="" all="Total")/printmiss; 
	where diffbudg in (0 1) and ms=1;
	format  hc3 handi.;
	run; 
	/*Retards de paiement*/
	proc tabulate data=base_etude_bms;
	class hc3 / preloadfmt missing order=data mlf ;
	var p_retpai /weight= poids_fin_ind_ms_fe  ;
	table (p_retpai="Retards de paiement"  *mean=' '*f=8.1 ) , (hc3="" all="Total")/printmiss; 
	where retpai in (0 1) and ms=1;
	format  hc3 handi.;
	run; 
	/*Difficult�s de logement*/
	proc tabulate data=base_etude_bms;
	class hc3 / preloadfmt missing order=data mlf ;
	var p_difflogt /weight= poids_fin_ind_ms_fe  ;
	table (p_difflogt="Difficult�s de logement"  *mean=' '*f=8.1 ) , (hc3="" all="Total")/printmiss; 
	where difflogt in (0 1) and ms=1;
	format  hc3 handi.;
	run; 

/*Graphique 2 - �tat de sant� d�clar� par les b�n�ficiaires de minimas sociaux et dans la population g�n�rale*/
	proc tabulate data=base_etude_bms;
	class hc3 hc1 hc2 hc4 / preloadfmt missing order=data mlf ;
	var eff  /weight=poids_fin_ind_ms_fe;
	table ( hc1="Etat de sant�" hc2 ="maladie chronique"  all="Ensemble") , (hc3="" all="Total")*( eff=""*f=8.0 ) /printmiss; 
	where  ms=1;
	format hc3 handi. hc1 hc1_. hc2 ouinon.;
	run; 


/*Graphique 3 - Relations familiales et amicales au cours des douze derniers mois des b�n�ficiaires de minima sociaux et dans la population g�n�rale */
	proc tabulate data=base_etude_bms;
	class hc3   / preloadfmt missing order=data mlf ;
	var  p_noami p_nofami p_fami p_ami  p_isol  /weight= poids_fin_ind_ms_fe ;
	table (p_nofami  ="Pas de famille " *mean=' '*f=8.1 
	p_noami  ="Pas d'ami " *mean=' '*f=8.1
	p_fami  ="Avoir vu ou communiqu� avec sa famille au moins une fois par mois" *mean=' '*f=8.1 
	p_ami="Avoir vu ou communiqu� avec ses au moins une fois par mois" *mean=' '*f=8.1  
	p_isol="Personnes isol�es de leur entourage et de leur famille" *mean=' '*f=8.1) , 
	(hc3="" all="Total")/printmiss; 
	where ms=1;
	format hc3 handi.;
	run; 


/* Tableau encadr� 3 - Limitations fonctionnelles des b�n�ficiaires de l�AAH et d�un minimum vieillesse �*/
	proc tabulate data=base_etude_bms;
	class aah17 minv17 / preloadfmt missing order=data mlf ;
	var 
	p_diff_tot p_diff_sev
		p_diff_phy_tot p_diff_phy_sev
			p_march pbcp_march 
			p_esca pbcp_esca  
			p_mach pbcp_mach

		p_diff_senso_tot p_diff_senso_sev
			p_voir pbcp_voir 
			p_ents pbcp_ents  
			p_entb pbcp_entb

		p_souv pqq_souv pbcp_souv


	/weight= poids_fin_ind_prest_FE;
	table 
	( 

	p_diff_tot ="Au moins une limitation fonctionnelle"*mean=' '*f=8.2 
	p_diff_sev ="Au moins une limitation fonctionnelle s�v�re"*mean=' '*f=8.2 

		p_diff_phy_tot="Au moins une difficult� physique"*mean=' '*f=8.2 
		p_diff_phy_sev="Au moins une difficult� physique s�v�re"*mean=' '*f=8.2 
			p_march ="Difficult�s � marcher 500m"*mean=' '*f=8.2 
			pbcp_march ="Difficult�s s�v�res � marcher 500m"*mean=' '*f=8.2 
			p_esca ="Difficult�s � monter ou descendre des marches"*mean=' '*f=8.2 
			pbcp_esca ="Difficult�s s�v�res � monter ou descendre des marches"*mean=' '*f=8.2
			p_mach ="Difficult�s � m�cher"*mean=' '*f=8.2 
			pbcp_mach ="Difficult�s s�v�res � m�cher"*mean=' '*f=8.2

		p_diff_senso_tot="Au moins une difficult� sensorielle"*mean=' '*f=8.2 
		p_diff_senso_sev="Au moins une difficult� sensorielle s�v�re"*mean=' '*f=8.2 
			p_voir ="Difficult�s pour voir"*mean=' '*f=8.2 
			pbcp_voir ="Difficult�s s�v�res pour voir"*mean=' '*f=8.2 
			p_ents ="Difficult�s � entendre dans une pi�ce silencieuse"*mean=' '*f=8.2 
			pbcp_ents ="Difficult�s s�v�res � entendre dans une pi�ce silencieuse"*mean=' '*f=8.2 
			p_entb ="Difficult�s � entendre dans une pi�ce bruyante"*mean=' '*f=8.2 
			pbcp_entb ="Difficult�s s�v�res � entendre dans une pi�ce bruyante"*mean=' '*f=8.2 

		p_souv ="Au moins une difficult� cognitive (se souvenir ou se concentrer)"*mean=' '*f=8.2 
		pbcp_souv ="Au moins une difficult� cognitive s�v�re(se souvenir ou se concentrer)"*mean=' '*f=8.2) , 
	(aah17="AAH" minv17="MINV" all="Total") /printmiss; 
	run; 
	/*nombre de difficult�s*/
	proc tabulate data=base_etude_bms;
	class  var_diff var_diff_sev aah17 minv17/ preloadfmt missing order=data mlf ;
	var eff  /weight= poids_fin_ind_ms_fe ;
	table ( var_diff="Limitation s�v�re ou mod�r�e" var_diff_sev="Limitation s�v�re ou mod�r�e s�v�re" all="Ensemble") , (aah17="aah" minv17="minv")* ( eff=""*f=8.0 ) /printmiss; 
	run; 

/*Tableau compl�mentaire A - Caract�ristiques des personnes handicap�es parmi les b�n�ficiaires de minima sociaux et dans l�ensemble de la population*/
	proc tabulate data=base_etude_bms;
	class hc3 AB2 age TYPE_MENAGE  AC1/ preloadfmt missing order=data mlf ;
	var eff/weight= poids_fin_ind_ms_fe ;
	table  (  AB2="Sexe"  age="Tranches d'�ge" TYPE_MENAGE="Type de m�nage"  AC1="Situation d'emploi" all="Ensemble"),
	(hc3="" all="Total")*( eff=' '*f=8.1 )  /printmiss; 
	where ms=1;
	format  AB2 sexe. age tr_age2_. TYPE_MENAGE typmen. HA2 dip. AC1 situemp. hc3 handi.;
	run; 
	/*ages moyens et m�dians*/
	/*selon situation vis � vis du handicap*/
	proc sort data=base_etude_bms; by hc3; run;
	proc univariate data=base_etude_bms; var age;by hc3; weight poids_fin_ind_ms_fe ; format hc3 handi.;run;
	

/*Tableau compl�mentaire B�- Part des b�n�ficiaires de minima sociaux par type de prestation selon qu�ils soient handicap�s ou non*/
	proc tabulate data=base_etude_bms;
	class hc3 age  AAH17  MINV17 RSA17  ASS17  PA17 / preloadfmt missing order=data mlf ;
	var eff/weight= poids_fin_ind_prest_FE  ;
	table   ( AAH17 ="AAH" MINV17="MINV" RSA17 ="RSA" ASS17="ASS"  PA17="PA"  all="total") ,  
	(hc3="" all="Total")*(eff= "" *f=8.1 )/printmiss; 
	where ms=1;
	format hc3 handi. ;
	run; 

/*Tableau compl�mentaire C�- Part des b�n�ficiaires pauvres en conditions de vie parmi les b�n�ficiaires de minima sociaux et dans la population g�n�rale*/
	/*Pour l'ensemble des BMS*/
	proc tabulate data=base_etude_bms;
	class hc3  age / preloadfmt missing order=data mlf ;
	var p_pcdv /weight= poids_fin_ind_ms_fe ;
	table ( p_pcdv  ="" *mean=' '*f=8.1 ) ,
	(hc3="" all="Total")* (age="" all="Ensemble")/printmiss; 
	where pcdv in (0 1) and ms=1;
	format hc3 handi. age tr_age1_.;
	run; 
	/*Par prestation*/
	proc tabulate data=base_etude_bms;
	class hc3 aah17  MINV17 RSA17 ASS17 age/ preloadfmt missing order=data mlf ;
	var p_pcdv /weight= poids_fin_ind_prest_FE;
	table (aah17="AAH"  MINV17="MINV" RSA17="RSA" ASS17="ASS")*( p_pcdv  ="" *mean=' '*f=8.1 ) , (hc3="" all="Total")* (age="" all="Ensemble")/printmiss; 
	where  pcdv in (0 1) and ms=1;
	format  hc3 handi. age tr_age1_.;
	run; 

/*Tableau compl�mentaire D - Types de dificult�s rencontr�es par dimension de pauvret� en conditions de vie*/
	/*Restrictions de consommation*/
	proc tabulate data=base_etude_bms;
	class hc3 / preloadfmt missing order=data mlf ;
	var p_temp p_vac p_meub p_vet p_viand p_rec p_kdo p_chauss p_repas /weight=poids_fin_ind_ms_fe;
	table 
	( 
	p_temp  ="Maintenir le logement � bonne temp�rature" *mean=' '*f=8.1 
	p_vac="Se payer uen semaine de vacances une fois par an"*mean=' '*f=8.1 
	p_meub="Remplacer des meubles hors d�usage"*mean=' '*f=8.1 
	p_vet="Acheter des v�tements neufs"*mean=' '*f=8.1 
	p_viand="Manger de la viande, du poulet ou du poisson (ou l��quivalent v�g�tarien) tous les deux jours"*mean=' '*f=8.1 
	p_rec="Recevoir des parents ou des amis"*mean=' '*f=8.1 
	p_kdo="Offrir des cadeaux � la famille ou aux amis une fois par an au moins"*mean=' '*f=8.1 
	p_chauss="Poss�der deux paires de bonnes chaussures (pour chaque adulte du m�nage)"*mean=' '*f=8.1
	p_repas="Passer une journ�e sans prendre au moins un repas complet, par manque d�argent"*mean=' '*f=8.1 ) , 
	(hc3="" all="Total")/printmiss;
	where  ms=1;
	format hc3 handi.;
	run;
	/*Insuffisance de ressources*/
	proc tabulate data=base_etude_bms;
	class hc3 / preloadfmt missing order=data mlf ;
	var p_remb p_dec  p_couv p_fin  p_eco  p_dif /weight=poids_fin_ind_ms_fe  ;
	table 
	( 
	p_remb  ="Part du remboursement d'emprunt sur le revenu sup�rieure � un tiers" *mean=' '*f=8.1 
	p_dec="D�couverts bancaires (tr�s souvent)"*mean=' '*f=8.1 
	p_couv="Couverture des d�penses par le revenu difficile"*mean=' '*f=8.1 
	p_fin="Aucun placement financier"*mean=' '*f=8.1 
	p_eco="Puiser dans ses �conomies"*mean=' '*f=8.1 
	p_dif="Opinion sur le niveau de vie : � C'est difficile, il faut s'endetter pour y arriver. �"*mean=' '*f=8.1  ) , 
	(hc3="" all="Total")/printmiss;
	format hc3 handi.;
	where  ms=1;
	run;
	/*Retards de paiement*/
	proc tabulate data=base_etude_bms;
	class hc3/ preloadfmt missing order=data mlf ;
	var  p_factures p_loyers  p_impot   /weight=poids_fin_ind_ms_fe  ;
	table 
	( 
	p_factures ="Retard de paiement de factures" *mean=' '*f=8.1 
	p_loyers="Retard de paiement du loyer"*mean=' '*f=8.1 
	p_impot="Retard de paiement des impots"*mean=' '*f=8.1   ) , (hc3="" all="Total")

	/printmiss;
	format  hc3 handi.;
	where  ms=1;
	run;
	/*Difficult�s de logement*/
	proc tabulate data=base_etude_bms;
	class hc3 
	 / preloadfmt missing order=data mlf ;
	var p_surpeup  p_sdb p_wc   p_hot p_chauf p_petit  p_difchauf p_hum  p_bruit   /weight=poids_fin_ind_ms_fe   ;
	table 
	( 
	p_surpeup ="Logement surpeupl�" *mean=' '*f=8.1 
	p_sdb     ="Absence de salle de bain" *mean=' '*f=8.1 
	p_wc      ="Absence de toilettes" *mean=' '*f=8.1 
	p_hot     ="Absence d'eau chaude" *mean=' '*f=8.1 
	p_chauf   ="Absence de chauffage central 'individuel ou collectif)" *mean=' '*f=8.1 
	p_petit   ="Logement trop petit" *mean=' '*f=8.1 
	p_difchauf="Logement trop difficile � chauffer" *mean=' '*f=8.1 
	p_hum     ="Logement humide" *mean=' '*f=8.1 
	p_bruit   ="Logement bruyant" *mean=' '*f=8.1    ) , (hc3="" all="Total") /printmiss;
	format  hc3 handi.;
	where  ms=1;
	run;

/*Tableau compl�mentaire E - Renoncement aux soins*/
	proc tabulate data=base_etude_bms;
	class hc3 / preloadfmt missing order=data mlf ;
	var p_renoncmed   p_renoncdent  /weight=poids_fin_ind_ms_fe   ;
	table 
	( p_renoncmed ="Renoncer � aller chez une m�decin pour des raisons financi�res" *mean=' '*f=8.1 
	  p_renoncdent="Renoncer � un soin dentaire pour des raisons financi�res" *mean=' '*f=8.1 ) , 
	(hc3="" all="Total")/printmiss;
	format  hc3 handi.;
	where  ms=1;
	run;

/*Tableau compl�mentaire F - Au cours du dernier mois, a �prouv� les sentiments suivants moins de la moiti� du temps ou de temps en temps*/
	proc tabulate data=base_etude_bms;
	class hc3   hc5_1 hc5_2 hc5_3 hc5_4 hc5_5 / preloadfmt missing order=data mlf ;
	var eff  /weight= poids_fin_ind_ms_fe;
	table ( hc5_1 ="Bonne humeur"  hc5_2="Calme et tranquille" hc5_3="Plein d'energie, vigoureux" hc5_4="Frais au reveil" hc5_5="Vie remplie de choses interessantes"  all="Ensemble") , 
	(hc3="" all="Total")* ( eff=""*f=8.0 ) /printmiss; 
	format  hc3 handi. hc5_1 hc5_2 hc5_3 hc5_4 hc5_5 freq_.;
	where  ms=1;
	run; 

/*Tableau compl�mentaire G�- Aides que pourraient recevoir ou apporter les b�n�ficiaires de  minima sociaux de/� son entourage*/
	/*Le b�n�ficiaire de minima sociaux peut compter sur son entourage pour :*/
	proc tabulate data=base_etude_bms;
	class hc3  age  hb8_1 hb8_2 hb8_3 / preloadfmt missing order=data mlf ;
	var eff  /weight= poids_fin_ind_ms_FE  ;
	table 
	( hb8_1= " Aide financi�re re�ue " hb8_2= " Aide mat�rielle" hb8_3 ="Soutien moral" all="Ensemble") , 
	(hc3="" all="Total")*( eff=""*f=8.0 ) /printmiss; 
	format   hc3 handi.  hb8_1 hb8_2 hb8_3  ouinon.;
	where ms=1;run; 
	/*L�entourage peut compter sur le b�n�ficiaire de minima sociaux  pour :*/
	proc tabulate data=base_etude_bms;
	class hc3  age  hb9_1 hb9_2 hb9_3 / preloadfmt missing order=data mlf ;
	var eff  /weight= poids_fin_ind_ms_FE  ;
	table 
	( hb9_1= " Aide financi�re " hb9_2= " Aide mat�rielle " hb9_3 ="Soutien moral" all="Ensemble") , 
	(hc3="" all="Total") *( eff=""*f=8.0 ) /printmiss; 
	format   hc3 handi.  hb9_1 hb9_2 hb9_3  ouinon.;
	where ms=1; 
	run; 
	/*Sentiment de solitude*/
	proc tabulate data=base_etude_bms;
	class hc3   hb7  / preloadfmt missing order=data mlf ;
	var eff  /weight=poids_fin_ind_ms_fe  ;
	table 
	( hb7 =" Sentiment de solitude" all="Ensemble") , 
	(hc3="" all="Total")* ( eff=""*f=8.0 ) /printmiss; 
	format  hc3 handi. hb7 hb7_.;
	where ms=1;
	run; 

